---
title: "5.4"
date: 2021-08-22T16:30:00-00:00
---

* [debianutils-5.4.tar.gz](../artifacts/debianutils-5.4.tar.gz)
* [debianutils-5.4.tar.gz.gpg](../artifacts/debianutils-5.4.tar.gz.gpg)
* [debianutils-5.4.tar.gz.sig](../artifacts/debianutils-5.4.tar.gz.sig)
